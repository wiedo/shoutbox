Shouts = new Mongo.Collection("shouts");

if (Meteor.isClient) {
  
  Template.body.helpers({
    shouts: function () {
      // Show newest tasks first
      return Shouts.find({}, {sort: {createdAt: -1}});
    }
});

  Template.body.events({
    'submit .shoutform': function (event) {
      var name = event.target.name.value;
      var text = event.target.text.value;
      
      Shouts.insert({
        name: name,
        text: text,
        createdAt: new Date() // current time
      });
      
      // Clear form
      event.target.text.value = "";

      // Prevent default form submit
      return false;
    },
    'click .delete': function (event) {
      Meteor.call('removeAllShouts')
    }
  });
  
  
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
    return Meteor.methods({

      removeAllShouts: function() {

        return Shouts.remove({});

      }

    });
  });
}
